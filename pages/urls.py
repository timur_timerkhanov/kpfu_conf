from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from . import views
from django.conf.urls import url

urlpatterns = [url(r'^$', views.entry_index, name='main_page'),
               url(r'^program/$', views.program, name='program'),
               url(r'^speakers/$', views.speakers, name='speakers'),
               url(r'^support/$', views.support, name='support'),
               url(r'^resettlement/$', views.resettlement, name='resettlement'),
               url(r'^about/$', views.about, name='about'),
               url(r'^registration/$', views.registration, name='registration'),
               url(r'^post/(?P<pk>[0-9]+)/$', views.post_detail, name='post_detail'),
               url(r'^kazan/$', views.kazan, name='kazan'),
               url(r'^tours/$', views.tours, name='tours'),
               ]

urlpatterns += staticfiles_urlpatterns()
