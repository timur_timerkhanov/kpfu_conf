import hashlib

from cloudinary.templatetags import cloudinary
from cloudinary import CloudinaryResource
from django import forms
from django.contrib import admin

from .models import *


class ImageInline(admin.TabularInline):
    model = Picture
    extra = 1


class PhotoInline(admin.TabularInline):
    model = Image
    extra = 1


class NewsForm(forms.ModelForm):
    class Meta:
        model = News
        fields = "__all__"


class NewsAdmin(admin.ModelAdmin):
    form = NewsForm

    class Media:
        js = (
            '/static/js/tinymce/jquery.tinymce.min.js',
            '/static/js/tinymce/tinymce.min.js',
            '/static/js/tinymce/tiny_mce_init.js',
        )


class ProgramAdmin(admin.ModelAdmin):
    class Media:
        js = (
            '/static/js/tinymce/jquery.tinymce.min.js',
            '/static/js/tinymce/tinymce.min.js',
            '/static/js/tinymce/tiny_mce_init.js',
        )


# Hotel

class HotelForm(forms.ModelForm):
    class Meta:
        model = Hotel
        fields = '__all__'


class HotelAdmin(admin.ModelAdmin):
    form = HotelForm

    def save_model(self, request, obj, form, change):
        if 'img' in form.changed_data:
            # CloudInary images
            s = str(obj.img).split('.')
            e = str()
            for i in range(0, len(s) - 1):
                e += s[i] + '.'
            filename = e[0:-1]
            print(filename)
            slash = filename.rfind("/")
            m = hashlib.md5()
            m.update(filename[slash + 1:].encode("UTF-8"))
            md5name = m.hexdigest()
            print(obj.img.name)
            obj.img.name = "123"
            obj.save()
        form.save(form)


# Sponsor
class SponsorForm(forms.ModelForm):
    class Meta:
        model = Sponsor
        fields = '__all__'


class SponsorAdmin(admin.ModelAdmin):
    form = SponsorForm

    def save_model(self, request, obj, form, change):
        if 'img' in form.changed_data:
            # CloudInary images
            s = str(obj.img).split('.')
            e = str()
            for i in range(0, len(s) - 1):
                e += s[i] + '.'
            filename = e[0:-1]
            print(filename)
            slash = filename.rfind("/")
            m = hashlib.md5()
            m.update(filename[slash + 1:].encode("UTF-8"))
            md5name = m.hexdigest()
            print(obj.img.name)
            obj.img.name = "123"
            obj.save()
        form.save(form)


class MemberCommitteeForm(forms.ModelForm):
    class Meta:
        model = Member_committee
        fields = '__all__'


class MemberCommitteeAdmin(admin.ModelAdmin):
    form = MemberCommitteeForm

    def save_model(self, request, obj, form, change):
        if 'img' in form.changed_data:
            # CloudInary images
            s = str(obj.img).split('.')
            e = str()
            for i in range(0, len(s) - 1):
                e += s[i] + '.'
            filename = e[0:-1]
            print(filename)
            slash = filename.rfind("/")
            m = hashlib.md5()
            m.update(filename[slash + 1:].encode("UTF-8"))
            md5name = m.hexdigest()
            print(obj.img.name)
            obj.img.name = "123"
            obj.save()
        form.save(form)

# Slider

class SliderAdmin(admin.ModelAdmin):
    inlines = [
        PhotoInline,
    ]

    def save_model(self, request, obj, form, change):
        if 'img' in form.changed_data:
            # CloudInary images
            s = str(obj.img).split('.')
            e = str()
            for i in range(0, len(s) - 1):
                e += s[i] + '.'
            filename = e[0:-1]
            print(filename)
            slash = filename.rfind("/")
            m = hashlib.md5()
            m.update(filename[slash + 1:].encode("UTF-8"))
            md5name = m.hexdigest()
            print(obj.img.name)
            obj.img.name = "123"
            obj.save()
        form.save(form)


class SpeakerAdmin(admin.ModelAdmin):
    class Media:
        js = (
            '/static/js/tinymce/jquery.tinymce.min.js',
            '/static/js/tinymce/tinymce.min.js',
            '/static/js/tinymce/tiny_mce_init.js',
        )


# Register your models here.
admin.site.register(Event)
admin.site.register(News, NewsAdmin)
admin.site.register(Program, ProgramAdmin)
admin.site.register(Speaker, SpeakerAdmin)

admin.site.register(Hotel, HotelAdmin)
admin.site.register(Member_committee, MemberCommitteeAdmin)
admin.site.register(Sponsor, SponsorAdmin)
admin.site.register(Slider, SliderAdmin)
