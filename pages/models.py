from django.db import models
from cloudinary.models import CloudinaryField


# Create your models here.


class Hotel(models.Model):
    name = models.CharField(max_length=50, verbose_name='название')
    address = models.CharField(max_length=100, verbose_name='адрес')
    site = models.CharField(max_length=100, verbose_name='сайт')
    phone = models.CharField(max_length=100, blank=True, verbose_name='телефон')
    price = models.CharField(max_length=100, verbose_name='стоимость')
    distance = models.CharField(max_length=20, verbose_name='расстояние')
    img = CloudinaryField(null=True, verbose_name='изображение')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Отель'
        verbose_name_plural = 'Отели'


class Sponsor(models.Model):
    name = models.CharField(max_length=100, verbose_name='название')
    description = models.TextField(verbose_name='описание')
    address = models.CharField(max_length=100, verbose_name='адрес')
    phone = models.CharField(max_length=100, verbose_name='телефон')
    email = models.CharField(max_length=100, verbose_name='почта')
    site = models.CharField(max_length=100, verbose_name='сайт', blank=True)
    img = CloudinaryField(null=True, verbose_name='изображение')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Спонсор'
        verbose_name_plural = 'Спонсоры'


class Member_committee(models.Model):
    role = models.CharField(max_length=50, blank=True, verbose_name='статус',
                            choices=(('Со-председатели:', 'председатель'), ('Координаторы:', 'координатор'),
                                     ('Члены оргкоммитета:', 'член оргкоммитета')))
    name = models.CharField(max_length=50, verbose_name='имя')
    description = models.TextField(verbose_name='описание')
    comment = models.CharField(blank=True, max_length=100, verbose_name='подробнее')
    img = CloudinaryField(null=True, blank=True, verbose_name='изображение')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Член Оргкоммитета'
        verbose_name_plural = 'Члены Оргкоммитета'


class News(models.Model):
    title = models.CharField(max_length=50, verbose_name='заголовок')
    text = models.TextField(verbose_name='текст')
    date = models.DateTimeField(auto_now_add=True, verbose_name='дата')
    preview = models.CharField(max_length=200, null=False, blank=False, verbose_name='Превью')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'


class Slider(models.Model):
    def __str__(self):
        return "слайдер"

    class Meta:
        verbose_name = 'Слайдер'
        verbose_name_plural = 'Слайдер'


class Image(models.Model):
    image = CloudinaryField(default='', blank=True, verbose_name='изображение')
    slider = models.ForeignKey(Slider, on_delete=models.CASCADE, verbose_name='слайдер')

    def __str__(self):
        return "изображение"

    class Meta:
        verbose_name = 'Изображение'
        verbose_name_plural = 'Изображения'


class Picture(models.Model):
    image = models.ImageField(default='', blank=True, verbose_name='изображение', upload_to='pages/static/img/news_img')
    news = models.ForeignKey(News, on_delete=models.CASCADE, verbose_name='новость')

    def __str__(self):
        return self.news.title

    class Meta:
        verbose_name = 'Изображение'
        verbose_name_plural = 'Изображения'


class Program(models.Model):
    text = models.TextField(verbose_name='текст')

    def __str__(self):
        return 'Программа'

    class Meta:
        verbose_name = 'Программа'
        verbose_name_plural = 'Программа'


class Speaker(models.Model):
    text = models.TextField(blank=True, verbose_name='текст')

    def __str__(self):
        return 'Пленарные докладчики'

    class Meta:
        verbose_name = 'Пленарные докладчики'
        verbose_name_plural = 'Пленарные докладчики'


class Event(models.Model):  # для главной страницы
    date_time = models.CharField(max_length=50, verbose_name='дата', blank=True)
    time = models.CharField(max_length=50, verbose_name='время', blank=True)
    info = models.CharField(max_length=50, verbose_name='информация')
    place = models.CharField(max_length=50, blank=True, verbose_name='место')

    def __str__(self):
        return str(self.info)

    class Meta:
        verbose_name = 'Дата'
        verbose_name_plural = 'Даты'
